execute 'apt-get update' do
  command 'apt-get update'
  user 'root'
  action :run
end

execute 'apt-get upgrade' do
  command 'apt-get upgrade -y -qq'
  user 'root'
  action :run
end

execute 'Install python-pip' do
  command 'apt install -y python-pip'
  user 'root'
end

execute 'Install python-pip' do
  command 'pip install --upgrade pip'
  user 'root'
end

execute "Install python-pip3" do
  command "apt install -y python3-pip"
  user "root"
end

execute "Install python3-dev" do
  command "apt install -y python3-dev"
  user "root"
end

execute "Install libffi-dev" do
  command "apt install -y libffi-dev"
  user "root"
end

execute "Install libssl-dev" do
  command "apt install -y libssl-dev"
  user "root"
end

execute "Install libxml2-dev" do
  command "apt install -y libxml2-dev"
  user "root"
end

execute "Install libxslt1-dev" do
  command "apt install -y libxslt1-dev"
  user "root"
end

execute "Install libjpeg8-dev" do
  command "apt install -y libjpeg8-dev"
  user "root"
end

execute "Install zlib1g-dev" do
  command "apt install -y zlib1g-dev"
  user "root"
end

execute "Install build-essential" do
  command "apt install -y build-essential"
  user "root"
end

execute "Install python3-cffi" do
  command "apt install -y python3-cffi"
  user "root"
end

package "zip" do
  action :install
end

package "unzip" do
  action :install
end

package 'virtualenv' do
  action :install
end

package 'nginx' do
  action :install
end

service 'nginx' do
  action [:start, :enable]
end

execute 'install aws_cli' do
  command 'pip install awscli'
  user 'root'
end

execute 'Upgrade awscli' do
  command 'pip install --upgrade awscli'
  user 'root'
end

group 'evesleep' do
end

user 'evesleep' do
  password '$1$tata$.y/rEeq60zUAVqnFPaSiZ/'
  group 'evesleep'
  shell '/bin/bash'
  home '/home/evesleep'
  manage_home true
  action :create
end

