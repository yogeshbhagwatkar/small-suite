#
# Cookbook:: csapp
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

#app = search("aws_opsworks_app", "shortname:smallsuite-backend").first
#env = app["environment"]
#mysql_host  = env["MYSQL_HOST"]

#Chef::Log.info("********** Database host is  '#{mysql_host}' **********")

include_recipe 'ss_backend_cookbook::setup'

directory '/tmp/project' do
  owner 'ubuntu'
  group 'ubuntu'
  mode  '755'
  action :create
end

bash "Run the configuraiton for eve" do
  user "root"
  code <<-EOH
   sudo su evesleep
   aws s3 cp s3://yogbucket/smallsuite-backend.zip   "/tmp/project"
  EOH
end

execute 'extract directory' do
  command 'sudo unzip /tmp/project/smallsuite-backend  -d /tmp/project/'
  user 'ubuntu'
end

directory '/opt/eve' do
  owner 'ubuntu'
  group 'ubuntu'
  mode  '755'
  action :create
end


execute 'copy data dir.' do
  command 'sudo rsync -arz /tmp/project/smallsuite-backend  /opt/eve/'
  user 'ubuntu'
end

#template "/opt/eve/smallsuite/ns_frontend_scripts/config.py" do
#    source "config.py.erb"
#    owner 'root'
#    group 'root'
#    mode '0755'
#    variables(
#       :mysql_host => env["MYSQL_HOST"],
#       :mysql_user => env["MYSQL_USER"],
#       :mysql_pass => env["MYSQL_PASSWORD"],
#       :mysql_db => env["MYSQL_DB"]
#    )
#end

directory "/tmp/project/smallsuite-backend" do
  recursive true
  action :delete
end

execute 'change the permission' do
  command 'chown evesleep. /opt/eve -R'
  user 'root'
end

bash "Run the configuraiton for eve" do
  user "root"
  code <<-EOH
   sudo su evesleep
   cd /opt/eve/smallsuite-backend
   virtualenv -p python3 ss-back-app
   chown evesleep. /opt/eve -R
   source ss-back-app/bin/activate
   pip3 install -r requirements.txt

   cd /opt/eve/smallsuite-backend/ss-back-app/bin
   pip3 install gunicorn
   pip3 install eventlet
   chown evesleep. /opt/eve -R
  EOH
end

cookbook_file '/etc/systemd/system/smallsuite-backend.service' do
  source 'smallsuite-backend.service'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

execute "reload service" do
  command "systemctl daemon-reload"
end

execute "start app service" do
  command "sudo systemctl start smallsuite-backend"
end

execute "status of App service" do
  command "systemctl status smallsuite-backend"
end

cookbook_file '/etc/nginx/sites-enabled/smallsuite-backend.conf' do
  source 'smallsuite-backend.conf'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

cookbook_file '/etc/nginx/sites-enabled/default' do
  source 'default'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

execute 'check nginx syntax' do
  command 'nginx -t'
  user 'root'
end

service 'nginx' do
  action :restart
end

