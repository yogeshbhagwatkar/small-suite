#
# Cookbook:: csapp
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

include_recipe 'smallsuite_cookbook::setup'

#app = search("aws_opsworks_app", "shortname:smallsuite").first
#env = app["environment"]
#mysql_host  = env["MYSQL_HOST"]
#
#Chef::Log.info("********** Database host is  '#{mysql_host}' **********")

directory '/tmp/project' do
  owner 'ubuntu'
  group 'ubuntu'
  mode  '755'
  action :create
end

bash "Run the configuraiton for eve" do
  user "root"
  code <<-EOH
   sudo su evesleep
   aws s3 cp s3://yogbucket/smallsuite.zip   "/tmp/project"
  EOH
end

execute 'extract directory' do
  command 'sudo unzip /tmp/project/smallsuite.zip  -d /tmp/project/'
  user 'ubuntu'
end

directory '/opt/eve' do
  owner 'ubuntu'
  group 'ubuntu'
  mode  '755'
  action :create
end


execute 'copy data dir.' do
  command 'sudo rsync -arz /tmp/project/smallsuite  /opt/eve/'
  user 'ubuntu'
end

#template "/opt/eve/smallsuite/ns_frontend_scripts/config.py" do
#    source "config.py.erb"
#    owner 'root'
#    group 'root'
#    mode '0755'
#    variables(
#       :mysql_host => env["MYSQL_HOST"],
#       :mysql_user => env["MYSQL_USER"],
#       :mysql_pass => env["MYSQL_PASSWORD"],
#       :mysql_db => env["MYSQL_DB"]
#    )
#end

directory "/tmp/project/smallsuite" do
  recursive true
  action :delete
end

execute 'change the permission' do
  command 'chown evesleep. /opt/eve -R'
  user 'root'
end

bash "Run the configuraiton for eve" do
  user "root"
  code <<-EOH
   sudo su evesleep
   cd /opt/eve/smallsuite
   virtualenv -p python3 ssapp
   chown evesleep. /opt/eve -R
   source ssapp/bin/activate
   pip3 install -r requirements.txt

   cd /opt/eve/smallsuite/ssapp/bin
   pip3 install gunicorn
   pip3 install eventlet
   chown evesleep. /opt/eve -R
  EOH
end

cookbook_file '/etc/systemd/system/smallsuite.service' do
  source 'smallsuite.service'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

execute "reload service" do
  command "systemctl daemon-reload"
end

execute "start app service" do
  command "sudo systemctl start smallsuite"
end

execute "status of App service" do
  command "systemctl status smallsuite"
end

cookbook_file '/etc/nginx/sites-enabled/smallsuite.conf' do
  source 'smallsuite.conf'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

cookbook_file '/etc/nginx/sites-enabled/default' do
  source 'default'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

execute 'check nginx syntax' do
  command 'nginx -t'
  user 'root'
end

service 'nginx' do
  action :restart
end

